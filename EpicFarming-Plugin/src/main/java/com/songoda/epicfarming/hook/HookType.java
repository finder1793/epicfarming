package com.songoda.epicfarming.hook;

public enum HookType {

    FACTION, TOWN, ISLAND, REGULAR
}
